//! Generated by `generate_integration_tests`, do not edit by hand.

use sourcegen::{project_root, skip_slow_tests};

#[test]
fn amplifier() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("AMPLIFIER").join("amplifier.va");
    super::test_compile(&root_file);
}
#[test]
fn asmhemt() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("ASMHEMT").join("asmhemt.va");
    super::test_compile(&root_file);
}
#[test]
fn bsim3() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIM3").join("bsim3.va");
    super::test_compile(&root_file);
}
#[test]
fn bsim4() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIM4").join("bsim4.va");
    super::test_compile(&root_file);
}
#[test]
fn bsim6() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIM6").join("bsim6.va");
    super::test_compile(&root_file);
}
#[test]
fn bsimbulk() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIMBULK").join("bsimbulk.va");
    super::test_compile(&root_file);
}
#[test]
fn bsimcmg() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIMCMG").join("bsimcmg.va");
    super::test_compile(&root_file);
}
#[test]
fn bsimimg() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIMIMG").join("bsimimg.va");
    super::test_compile(&root_file);
}
#[test]
fn bsimsoi() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("BSIMSOI").join("bsimsoi.va");
    super::test_compile(&root_file);
}
#[test]
fn cccs() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("CCCS").join("cccs.va");
    super::test_compile(&root_file);
}
#[test]
fn current_source() {
    if skip_slow_tests() {
        return;
    }
    let root_file =
        project_root().join("integration_tests").join("CURRENT_SOURCE").join("current_source.va");
    super::test_compile(&root_file);
}
#[test]
fn diode() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("DIODE").join("diode.va");
    super::test_compile(&root_file);
}
#[test]
fn diode_cmc() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("DIODE_CMC").join("diode_cmc.va");
    super::test_compile(&root_file);
}
#[test]
fn ekv() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("EKV").join("ekv.va");
    super::test_compile(&root_file);
}
#[test]
fn ekv_longchannel() {
    if skip_slow_tests() {
        return;
    }
    let root_file =
        project_root().join("integration_tests").join("EKV_LONGCHANNEL").join("ekv_longchannel.va");
    super::test_compile(&root_file);
}
#[test]
fn hicuml2() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("HICUML2").join("hicuml2.va");
    super::test_compile(&root_file);
}
#[test]
fn hisim2() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("HiSIM2").join("hisim2.va");
    super::test_compile(&root_file);
}
#[test]
fn hisimhv() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("HiSIMHV").join("hisimhv.va");
    super::test_compile(&root_file);
}
#[test]
fn hisimsotb() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("HiSIMSOTB").join("hisimsotb.va");
    super::test_compile(&root_file);
}
#[test]
fn mextram() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("MEXTRAM").join("mextram.va");
    super::test_compile(&root_file);
}
#[test]
fn mvsg_cmc() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("MVSG_CMC").join("mvsg_cmc.va");
    super::test_compile(&root_file);
}
#[test]
fn psp102() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("PSP102").join("psp102.va");
    super::test_compile(&root_file);
}
#[test]
fn psp103() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("PSP103").join("psp103.va");
    super::test_compile(&root_file);
}
#[test]
fn resistor() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("RESISTOR").join("resistor.va");
    super::test_compile(&root_file);
}
#[test]
fn vccs() {
    if skip_slow_tests() {
        return;
    }
    let root_file = project_root().join("integration_tests").join("VCCS").join("vccs.va");
    super::test_compile(&root_file);
}
